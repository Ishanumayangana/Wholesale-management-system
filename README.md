Wholesale Management System
Overview

This is a Wholesale Management System designed to facilitate the management of wholesale operations. The system is built using C# and WPF (Windows Presentation Foundation) for the user interface. It provides functionalities for managing products, customers, orders, inventory, and sales.
Features

    Product Management: Add, edit, delete products from the inventory.
    Customer Management: Maintain a database of customers including their information.
    Order Management: Create, edit, and track orders placed by customers.
    Inventory Management: Keep track of available products in the inventory.
    Sales Reporting: Generate reports on sales, orders, and inventory status.
    User Authentication: Secure login system to control access to the system.
    Data Persistence: Data is stored persistently to ensure information is retained across sessions.

Usage

    Login: Upon launching the application, users will be prompted to login with valid credentials.
    Dashboard: After successful login, users will be directed to the dashboard displaying summary information.
    Navigate: Use the navigation menu to access different modules such as products, customers, orders, etc.
    Manage Data: Add, edit, delete data as required in each module.
    Generate Reports: Utilize the reporting functionality to generate insights into sales, orders, and inventory.
    Logout: Ensure to logout from the system once done to maintain security.
